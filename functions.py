from preprocessing import NLTKPreprocessor


#reads a file
#outputs a parsed document ready for sikit learn
#writes a file to file directory
def preprocessAndOutText(rfile):
    with open(rfile) as f:
        documents = f.read().splitlines()
        p = NLTKPreprocessor()
        #you can change the preprocessing here
        documentNonParsed = p.transform(documents)
        documentsparsed = p.inverse_transform(documentNonParsed)
        #you can change output file name
        thefile = open('outputReadableNoNumbers.txt', 'w')
        for item in documentsparsed:
            thefile.write("%s\n" % item)
        return documentsparsed

#reads a file for sikit learn
#outputs a parsed document ready for sikit learn
def loadPreprocessedText(rfile):
    with open(rfile) as f:
        documents = f.read().splitlines()
        return documents

#reads a file
#outputs a parsed document ready for sikit learn
#no text output
def preprocessText(rfile):
    with open(rfile) as f:
        documents = f.read().splitlines()
        p = NLTKPreprocessor()
        documentsparsed = p.inverse_transform(p.transform(documents))
        return documentsparsed

#reads a file
#outputs a parsed document of labels ready for sikit learn
def getPolarities(pfile):
    with open(pfile) as g:
       polarities = g.read().splitlines()
       return polarities


#reads a file
#outputs a list of files
#writes a file to file directory
def createpreprocessAndOutText(rfile):
    #lower=True, strip=True, lemmatiz=True, names=True, numbers=True, adv=True, conjonction=True
    filenames = []
    filenames.append(rfile)
    with open(rfile) as f:
        documents = f.read().splitlines()
    for x in range(0, 2):
            low = False
            if(x == 1):
                low = True
            for l in range(0, 2):
                str = False
                if (l == 1):
                    str = True
                for m in range(0, 2):
                    lem = False
                    if (m == 1):
                        lem = True
                    for n in range(0, 2):
                        nam = False
                        if (n == 1):
                            nam = True
                        for o in range(0, 2):
                            num = False
                            if (o == 1):
                                num = True
                            for p in range(0, 2):
                                ad = False
                                if (p == 1):
                                    ad = True
                                for q in range(0, 2):
                                    if (q == 0):
                                        con = False
                                        p = NLTKPreprocessor(lower=low, strip=str, lemmatiz=lem, names=nam,
                                                         numbers=num, adv=ad, conjonction=con)
                                        documentNonParsed = p.transform(documents)
                                        documentsparsed = p.inverse_transform(documentNonParsed)
                                        filename = "outputlower%sstrip%slemmatiz%snames%snumbers%sadverbs%sconjoction%s.txt" % (low,str,lem,nam,num,ad,con)
                                        filenames.append(filename)
                                        thefile = open(filename, 'w')
                                        for item in documentsparsed:
                                            thefile.write("%s\n" % item)
                                    if (q == 1):
                                        con = True
                                        p = NLTKPreprocessor(lower=low, strip=str, lemmatiz=lem, names=nam,
                                                         numbers=num, adv=ad, conjonction=con)
                                        documentNonParsed = p.transform(documents)
                                        documentsparsed = p.inverse_transform(documentNonParsed)
                                        filename = "outputlower%sstrip%slemmatiz%snames%snumbers%sadverbs%sconjoction%s.txt" % (
                                        low, str, lem, nam, num, ad, con)
                                        filenames.append(filename)
                                        thefile = open(filename, 'w')
                                        for item in documentsparsed:
                                            thefile.write("%s\n" % item)
    return filenames