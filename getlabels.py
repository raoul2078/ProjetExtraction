import glob
from difflib import SequenceMatcher

def main():
    FilesToCSV()
    x = 1
    print("starting...")
    #document a libelé
    DocumentName = "test_data.csv"
    with open(DocumentName) as f:
        print("document opened...")
        documents = f.read().splitlines()
        thefile = open('TrueLabels.csv', 'w')
        for d in documents:
            # document a Positf
            DocumentNamePos = "Pos.csv"
            # document a Negatif
            DocumentNameNeg = "Neg.csv"
            textFound = False
            with open(DocumentNamePos) as P:
                documentsP = P.read().splitlines()
                for p in documentsP:
                    if (p==d):
                        thefile.write("1\n")
                        textFound = True
                        break
            if (textFound==False):
                with open(DocumentNameNeg) as N:
                    documentsN = N.read().splitlines()
                    for n in documentsN:
                        if (n==d):
                            thefile.write("-1\n")
                            print("-1")
                            textFound = True
                            break
            if(textFound==False):
                thefile.write("0\n")
                print("0")
            print("%s/4000 percent finished" % x)
            x=x+1

def FilesToCSV():
    print("writing text to csv...")
    namesneg = glob.glob("neg/*.txt")
    namespos = glob.glob("pos/*.txt")
    namesneg1 = glob.glob("neg1/*.txt")
    namespos1 = glob.glob("pos1/*.txt")
    namesneg2 = glob.glob("neg2/*.txt")
    namespos2 = glob.glob("pos2/*.txt")
    namesneg3 = glob.glob("neg3/*.txt")
    namespos3 = glob.glob("pos3/*.txt")
    thefilepos = open('Pos.csv', 'w')
    thefileneg = open('Neg.csv', 'w')
    for nameN in namesneg:
        with open(nameN) as N:
            text = N.read()
            thefileneg.write("%s\n" % text)
    print("first loop over...")
    for nameP in namespos:
        with open(nameP) as P:
            text = P.read()
            thefilepos.write("%s\n" % text)
    print("second loop over...")
    for nameN in namesneg1:
        with open(nameN) as N:
            text = N.read()
            thefileneg.write("%s\n" % text)
    print("first loop over...")
    for nameP in namespos1:
        with open(nameP) as P:
            text = P.read()
            thefilepos.write("%s\n" % text)
    print("second loop over...")
    for nameN in namesneg2:
        with open(nameN) as N:
            text = N.read()
            thefileneg.write("%s\n" % text)
    print("first loop over...")
    for nameP in namespos2:
        with open(nameP) as P:
            text = P.read()
            thefilepos.write("%s\n" % text)
    print("second loop over...")
    for nameN in namesneg3:
        with open(nameN) as N:
            text = N.read()
            thefileneg.write("%s\n" % text)
    print("first loop over...")
    for nameP in namespos3:
        with open(nameP) as P:
            text = P.read()
            thefilepos.write("%s\n" % text)
    print("second loop over...")


if __name__ == "__main__":
    main()