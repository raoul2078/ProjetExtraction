# Projet Extraction de connaissances
## Master1, Université de Montpellier
### Les éléments principaux du projet
* __compareModel:__ Fonction pour créer [le fichier de comparaison](https://docs.google.com/spreadsheets/d/1ef0cDyr0IeRliJjD4AwrzluiFN_sbVuarDobyoZU19g/edit?usp=sharing)
* __extraction:__ Fonction pour créer [le fichier de LogisticRegression](https://docs.google.com/spreadsheets/d/1c-HTJru5UARyVC9pnhSJCho2xf7Ejy3dEw0fwbfBGDE/edit?usp=sharing)
* __extractionNaiveBayes:__ Fonction pour créer un model avec NaiveBayes
* __extractSVCLinear:__ Fonction pour créer [le fichier de SVClinear](https://docs.google.com/spreadsheets/d/1j0AaX_6L1kbOlzBmcHgw7E30KN28O9IP6wmTVvCglhQ/edit?usp=sharing)
* __functions:__ fichier avec les fonctions pour le main (ouverture-pretraitement-ecriture)
* __getlabels:__ programme qui créer deux documents avec les dataset trouver sur l'internet 1 positif et un négatif 
* __LinearSVC:__ fonction pour créer un model avec SVCLinear
* __main:__ Programme pour exécuter toutes nos fonctions
* __mainrendu:__ Programme spécifique pour le rendu du challenge
* __preprocessing:__ Fonction/Class pour le traitement des documents 
* __predication.csv:__ resultat pour le challenge
* __csv resultat:__ resultat pour creation des tableurs
* __neg*:__ des commantaires negatif tiré de l'internet
* __pos*:__ des commantaires positif tiré de l'internet
* __Neg.csv:__ les commantaires negatif tiré de l'internet en format csv
* __Pos.csv:__ les commantaires positif tiré de l'internet en format csv
* __fulldataset:__ nos dataset pretraité
