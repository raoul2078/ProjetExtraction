from sklearn.pipeline import Pipeline
from sklearn.preprocessing import LabelEncoder
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split as tts
from sklearn.metrics import classification_report as clsr
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_auc_score
import numpy as np

def extract(Document,Label):
    print("creating model...")
    #Separation into training documents and test documents
    document_train, document_test, labels_train, labels_test = tts(Document, Label, test_size=0.2)
    #vectorisation without preprocessing (already done)
    vect = TfidfVectorizer(min_df=4, ngram_range = (1,4), preprocessor=None, lowercase=False).fit(document_train)
    #Print of the words
    print("Number of feature words :")
    print(len(vect.get_feature_names()))
    print()
    #document in vector form |(phrase,word) -positive and negative coefficients-|
    document_train_vectorized = vect.transform(document_train)
    #model creation
    model = LogisticRegression()
    model.fit(document_train_vectorized, labels_train)
    #Test on the tts
    predictions = model.predict(vect.transform(document_test))
    #print results
    print(clsr(labels_test, predictions))

    feature_names = np.array(vect.get_feature_names())
    sorted_coef_index = model.coef_[0].argsort()
    print('Smallest Coefs: \n{}\n'.format(feature_names[sorted_coef_index[:20]]))
    print('Largest Coefs: \n{}\n'.format(feature_names[sorted_coef_index[:-21:-1]]))

def extractToDocument(Document,Label):
    results = ""
    for df in range(2, 7):
        for ngr in range(1, 5):
            name = ",%s,%s" % (df,ngr)
            results = results + name
            print(name)
            for times in range(0, 9):
                if(ngr == 1):
                    Labels = encodeLabels(Label)
                    document_train, document_test, labels_train, labels_test = tts(Document, Labels, test_size=0.2)
                    vect = TfidfVectorizer(min_df=df, preprocessor=None, lowercase=False).fit(document_train)
                    document_train_vectorized = vect.transform(document_train)
                    model = LogisticRegression()
                    model.fit(document_train_vectorized, labels_train)
                    predictions = model.predict(vect.transform(document_test))
                    result = ",%s,%s" % (len(vect.get_feature_names()), roc_auc_score(labels_test, predictions))
                    results = results + result
                if(ngr!=1):
                    Labels = encodeLabels(Label)
                    document_train, document_test, labels_train, labels_test = tts(Document, Labels, test_size=0.2)
                    vect = TfidfVectorizer(min_df=df, ngram_range=(1, ngr), preprocessor=None, lowercase=False).fit(
                        document_train)
                    document_train_vectorized = vect.transform(document_train)
                    model = LogisticRegression()
                    model.fit(document_train_vectorized, labels_train)
                    predictions = model.predict(vect.transform(document_test))
                    result = ",%s,%s" % (len(vect.get_feature_names()), roc_auc_score(labels_test, predictions))
                    results = results + result
    return results




def encodeLabels(Label):
    labels = LabelEncoder()
    return labels.fit_transform(Label)