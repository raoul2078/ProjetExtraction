from sklearn.pipeline import Pipeline
from sklearn.preprocessing import LabelEncoder
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split as tts
from sklearn.metrics import classification_report as clsr
from sklearn.linear_model import LogisticRegression
from sklearn import datasets
from sklearn.metrics import roc_auc_score
from sklearn.naive_bayes import BernoulliNB
from math import sqrt
import numpy as np

def extractNaiveBayes(Document,Label):
    print("creating model...")
    #creation label
    #Labels = encodeLabels(Label)
    #Separation into training documents and test documents
    document_train, document_test, labels_train, labels_test = tts(Document, Label, test_size=0.2)
    #vectorisation without preprocessing (already done)
    vect = TfidfVectorizer(min_df=2, ngram_range = (1,3), preprocessor=None, lowercase=False).fit(document_train)
    #Print of the words
    print("Number of feature words :")
    print(len(vect.get_feature_names()))
    print()
    #document in vector form |(phrase,word) -positive and negative coefficients-|
    document_train_vectorized = vect.transform(document_train)
    #model creation
    model = BernoulliNB()
    model.fit(document_train_vectorized, labels_train)
    # Test on the tts
    predictions = model.predict(vect.transform(document_test))
    # print results
    print(clsr(labels_test, predictions))



def encodeLabels(Label):
    labels = LabelEncoder()
    return labels.fit_transform(Label)